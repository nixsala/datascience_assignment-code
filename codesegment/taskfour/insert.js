var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("dataweather");
  var myobj = { AverageTemperature: "30", AverageTemperatureUncertainty: "65",City:"guater", Country: "SouthAfrica",Latitude: "12.01", Longitude: "20.1" };
  dbo.collection("globaltemp").insertOne(myobj, function(err, res) {
    if (err) throw err;
    console.log("1 document inserted");
    db.close();
  });
});

